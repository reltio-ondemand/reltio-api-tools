```
#!txt

Copyright 2016 Reltio 100 Marine Parkway, Suite 275, Redwood Shores, CA USA 94065 (855) 360-DATA www.reltio.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
  
[TOC]




# Welcome

Welcome to the project - Reltio LCA Samples!

## Purposes

The following is a collection of utilities meant to make the Reltio API more usable by end users.

## Service architecture

The Reltio API Operations contained within are standalone java programs which can be run on a client machine to interact with Reltio.

# Reltio API Operations #

## Dataload Processor ##

This program is used to load bulk data into the Reltio environment.



# Additional information

For more information, please visit 

## [Dataload Processor](https://bitbucket.org/reltio-ondemand/reltio-api-tools/src/master/dataload-processor/?at=master)
