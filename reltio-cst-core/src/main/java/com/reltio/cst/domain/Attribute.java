package com.reltio.cst.domain;

import java.io.Serializable;

/**
 * 
 * 
 * This the base class of Reltio API attribute and contains all required
 * properties inside any attribute
 * 
 *
 */
public class Attribute implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2064606123513291149L;

	private String type;
	private Object value;
	private Boolean dataProvider;
	private String uri;
	private String createDate;
	private String updateDate;
	private String deleteDate;
	private String objectURI;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public Boolean getDataProvider() {
		return dataProvider;
	}

	public void setDataProvider(Boolean dataProvider) {
		this.dataProvider = dataProvider;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getDeleteDate() {
		return deleteDate;
	}

	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}

	public String getObjectURI() {
		return objectURI;
	}

	public void setObjectURI(String objectURI) {
		this.objectURI = objectURI;
	}

}
