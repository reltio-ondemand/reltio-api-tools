package com.reltio.cst.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Map;

import javax.net.ssl.*;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.properties.AuthenticationProperties;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.util.GenericUtilityService;

/**
 * 
 * This Rest API service impl class using the direct Java API for Http Calls
 * 
 *
 */
public class SimpleRestAPIServiceImpl implements RestAPIService {

	/*static {

		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()

		{
			@Override
			public boolean verify(String hostname, SSLSession arg1) {

				// TODO Auto-generated method stub

				return true;
				//return false;

			}
		});
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#get(java.lang.String,
	 * java.util.Map)
	 */
	@Override
	public String get(String requestUrl, Map<String, String> requestHeaders)
			throws APICallFailureException, GenericException {

		return doExecute(requestUrl, requestHeaders, null, HttpMethod.GET);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#post(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String post(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException {

		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.POST);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#put(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String put(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException {

		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.PUT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#delete(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String delete(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws APICallFailureException,
			GenericException {
		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.DELETE);
	}

	/**
	 * 
	 * This is common method to do all the different http method API calls
	 * 
	 * @param requestUrl
	 * @param requestHeaders
	 * @param requestBody
	 * @param requestMethod
	 * @return
	 * @throws GenericException
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#doExecute(java.lang.String,
	 * java.util.Map, java.lang.String, com.reltio.cst.domain.HttpMethod)
	 */
	@Override
	public String doExecute(String requestUrl,
			Map<String, String> requestHeaders, String requestBody,
			HttpMethod requestMethod) throws APICallFailureException,
			GenericException {

		String response = null;
		URL url = null;
		HttpsURLConnection connection = null;
		InputStream inputStream = null;

		try {
			SSLContext context = SSLContext.getInstance("TLSv1.2");
			context.init(null,null,null);

			// CREATE URL connection for String request url
			url = new URL(null, encodeURL(requestUrl),
					new sun.net.www.protocol.https.Handler());
			connection = (HttpsURLConnection) url.openConnection();
			connection.setSSLSocketFactory(context.getSocketFactory());

			connection.setRequestMethod(requestMethod.name());
			// Add all the header parameters to the URL
			if (requestHeaders != null) {
				for (Map.Entry<String, String> headerEntrySet : requestHeaders
						.entrySet()) {
					connection.setRequestProperty(headerEntrySet.getKey(),
							headerEntrySet.getValue());
				}
			}

			// Add the request Body to the API call if it is not GET method
			if (!requestMethod.equals(HttpMethod.GET)) {
				// Add Content Type Header
				connection.setRequestProperty(
						AuthenticationProperties.CONTENT_TYPE_HEADER,
						AuthenticationProperties.CONTENT_TYPE_JSON);

				connection.setDoOutput(true);
				OutputStream os =  connection.getOutputStream();
				if (requestBody == null) {
					requestBody = "";
				}
				try {
					os.write(requestBody.getBytes("UTF-8"));
				} finally {
					try {
						os.close();
					} catch (IOException logOrIgnore) {
					}
				}
			}

			// Verify the Response
			int errorCode = connection.getResponseCode();

			if (errorCode >= 400) {
				inputStream = connection.getErrorStream();
				String errorResponse = readResponse(inputStream);
				if (errorCode != 401) {
					System.out.println("REST Api call failed... Response URL: "
							+ requestUrl + " | Error Code: " + errorCode
							+ " | Error Response: "
							+ connection.getResponseMessage()
							+ " |||| Error Message body: " + errorResponse);
				}
				throw new APICallFailureException(errorCode, errorResponse);
			} else {
				inputStream = connection.getInputStream();
				// if the API call completed successfully
				response = readResponse(inputStream);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new GenericException(e.getLocalizedMessage());
		} catch (APICallFailureException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new GenericException(e.getLocalizedMessage());
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (connection != null) {
				connection.disconnect();
			}
		}

		return response;
	}

	/**
	 * 
	 * This is the helper method to read the response from API call.
	 * 
	 * @return
	 */
	private String readResponse(InputStream inputStream) {

		StringBuilder builder = new StringBuilder();
		try {
			if (inputStream != null) {
				byte[] buffer = new byte[100000];
				int count = inputStream.read(buffer);
				while (count > 0) {
					builder.append(new String(buffer, 0, count));
					count = inputStream.read(buffer);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return builder.toString();
	}

	private String encodeURL(String url) throws GenericException {

		URL encodeUrl = null;
		String newUrlStr = url;
		try {
			encodeUrl = new URL(url);
			String query = encodeUrl.getQuery();
			if (!GenericUtilityService.checkNullOrEmpty(query)) {
				
				String encodedQuery = "";
				String[] queryParams = query.split("&");
				System.out.println(query);
				for (String param : queryParams) {
					int index = param.indexOf("=");
					if (!encodedQuery.isEmpty()) {
						encodedQuery += "&";
					}
					encodedQuery += param.substring(0, index)
							+ "="
							+ URLEncoder.encode(param.substring(index + 1),
									"UTF-8");
				}

				newUrlStr = encodeUrl.getProtocol() + "://"
						+ encodeUrl.getAuthority() + encodeUrl.getPath() + "?"
						+ encodedQuery;
				
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new GenericException(
					"Invalid URL... Verify the URL and provide the correct one...");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		System.out.println(newUrlStr);

		return newUrlStr;
	}
}
