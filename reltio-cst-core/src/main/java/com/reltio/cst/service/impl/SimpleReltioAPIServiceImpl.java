package com.reltio.cst.service.impl;

import java.util.HashMap;
import java.util.Map;

import com.reltio.cst.domain.HttpMethod;
import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.properties.AuthenticationProperties;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.RestAPIService;
import com.reltio.cst.service.TokenGeneratorService;
import com.reltio.cst.util.GenericUtilityService;

/**
 * 
 * This class provides an easy way to do the Reltio API calls. It implements all
 * required HTTP methods like GET,PUT,POST & DELETE
 * 
 *
 */
public class SimpleReltioAPIServiceImpl implements ReltioAPIService {

	/**
	 * Static instance for Rest API service class
	 */
	private static RestAPIService restAPIService = new SimpleRestAPIServiceImpl();

	/**
	 * Instance variable for generating token
	 */
	private TokenGeneratorService tokenGeneratorService;

	private static final int initialRetryCount = 0;

	public SimpleReltioAPIServiceImpl(
			TokenGeneratorService tokenGeneratorService) {
		this.tokenGeneratorService = tokenGeneratorService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#get(java.lang.String,
	 * java.util.Map)
	 */
	@Override
	public String get(String requestUrl, Map<String, String> requestHeaders)
			throws ReltioAPICallFailureException, GenericException {

		return doExecute(requestUrl, requestHeaders, null, HttpMethod.GET,
				initialRetryCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#post(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String post(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws GenericException,
			ReltioAPICallFailureException {
		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.POST, initialRetryCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#put(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String put(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws ReltioAPICallFailureException,
			GenericException {
		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.PUT, initialRetryCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#delete(java.lang.String,
	 * java.util.Map, java.lang.String)
	 */
	@Override
	public String delete(String requestUrl, Map<String, String> requestHeaders,
			String requestBody) throws ReltioAPICallFailureException,
			GenericException {
		return doExecute(requestUrl, requestHeaders, requestBody,
				HttpMethod.DELETE, initialRetryCount);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.ReltioAPIService#get(java.lang.String)
	 */
	@Override
	public String get(String requestUrl) throws ReltioAPICallFailureException,
			GenericException {
		return get(requestUrl, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.ReltioAPIService#post(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String post(String requestUrl, String requestBody)
			throws ReltioAPICallFailureException, GenericException {
		return post(requestUrl, null, requestBody);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.ReltioAPIService#put(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String put(String requestUrl, String requestBody)
			throws ReltioAPICallFailureException, GenericException {
		return put(requestUrl, null, requestBody);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.ReltioAPIService#delete(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String delete(String requestUrl, String requestBody)
			throws GenericException, ReltioAPICallFailureException {

		return delete(requestUrl, null, requestBody);
	}

	/**
	 * @param requestUrl
	 * @param requestHeaders
	 * @param requestBody
	 * @param httpMethod
	 * @param retryCount
	 * @return
	 * @throws APICallFailureException
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	private String doExecute(String requestUrl,
			Map<String, String> requestHeaders, String requestBody,
			HttpMethod httpMethod, int retryCount) throws GenericException,
			ReltioAPICallFailureException {

		String responseStr = null;

		// Validate the input
		if (GenericUtilityService.checkNullOrEmpty(requestUrl)
				|| httpMethod == null) {
			throw new GenericException(
					"Invalid Input Parameters... Provide proper Request URL/Http Method");
		}

		// Check whether header is not null
		if (requestHeaders == null) {
			requestHeaders = new HashMap<String, String>();
		}

		try {
			// Add Reltio Authorization Token in the header
			addReltioToken(requestHeaders);
			responseStr = restAPIService.doExecute(requestUrl, requestHeaders,
					requestBody, httpMethod);
		} catch (APICallFailureException e) {

			if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
				// Add Reltio Authorization Token in the header
				addNewReltioToken(requestHeaders);
				responseStr = doExecute(requestUrl, requestHeaders,
						requestBody, httpMethod, retryCount + 1);
			} else {

				System.out
						.println("Reltio API returned an Exception While executing the request... Request URL: "
								+ requestUrl
								+ " |||| Error Code : "
								+ e.getErrorCode()
								+ " |||| Error Message: "
								+ e.getErrorResponse());

				throw new ReltioAPICallFailureException(e);
			}

		} catch (GenericException e) {

			if (retryCount < AuthenticationProperties.RETRY_LIMIT) {
				// Add Reltio Authorization Token in the header
				addNewReltioToken(requestHeaders);
				responseStr = doExecute(requestUrl, requestHeaders,
						requestBody, httpMethod, retryCount + 1);
			} else {
				System.out
						.println("Unexcepted Error While executing the request... Request URL: "
								+ requestUrl
								+ " |||| Exception Message: "
								+ e.getExceptionMessage());
				throw e;
			}

		}

		return responseStr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.reltio.cst.service.RestAPIService#doExecute(java.lang.String,
	 * java.util.Map, java.lang.String, com.reltio.cst.domain.HttpMethod)
	 */
	@Override
	public String doExecute(String requestUrl,
			Map<String, String> requestHeaders, String requestBody,
			HttpMethod requestMethod) throws GenericException,
			ReltioAPICallFailureException {
		return doExecute(requestUrl, requestHeaders, requestBody,
				requestMethod, initialRetryCount);
	}

	/**
	 * @param requestHeaders
	 * @throws APICallFailureException
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	private void addReltioToken(Map<String, String> requestHeaders)
			throws GenericException, ReltioAPICallFailureException {
		// Add Reltio Authorization Token in the header
		try {
			requestHeaders.put(AuthenticationProperties.AUTH_SERVER_HEADER,
					AuthenticationProperties.AUTH_SERVER_API_CALL_TOKEN_PPREFIX
							+ tokenGeneratorService.getToken());
		} catch (APICallFailureException e) {
			e.printStackTrace();
			throw new ReltioAPICallFailureException(e);
		}
	}

	/**
	 * @param requestHeaders
	 * @throws APICallFailureException
	 * @throws GenericException
	 * @throws ReltioAPICallFailureException
	 */
	private void addNewReltioToken(Map<String, String> requestHeaders)
			throws GenericException, ReltioAPICallFailureException {
		// Add Reltio Authorization Token in the header
		try {
			requestHeaders.put(AuthenticationProperties.AUTH_SERVER_HEADER,
					AuthenticationProperties.AUTH_SERVER_API_CALL_TOKEN_PPREFIX
							+ tokenGeneratorService.getNewToken());
		} catch (APICallFailureException e) {
			e.printStackTrace();
			throw new ReltioAPICallFailureException(e);
		}
	}
}
