package com.reltio.file;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tika.parser.txt.CharsetDetector;
import org.apache.tika.parser.txt.CharsetMatch;

public class ReltioFlatFileReader implements ReltioFileReader {

	private final BufferedReader fileReader;
	private final String seperator;

	public ReltioFlatFileReader(String fileName, String seperator)
			throws IOException {
		BufferedInputStream isr = new BufferedInputStream(new FileInputStream(
				StringEscapeUtils.escapeJava(fileName)));

		CharsetDetector charsetDetector = new CharsetDetector();
		charsetDetector.setText(isr);
		charsetDetector.enableInputFilter(true);
		CharsetMatch cm = charsetDetector.detect();
		System.out.println("Decorder of the" + fileName + "(CharSet) :: "
				+ cm.getName());
		isr.close();

		if (Charset.availableCharsets().get(cm.getName()) == null
				|| cm.getName().equals("Shift_JIS")) {
			System.out
					.println("The "
							+ cm.getName()
							+ " charset not supported. So letting the reader to choose apporiate the Charset...");
			fileReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(
							StringEscapeUtils.escapeJava(fileName))));
			// BOM marker will only appear on the very beginning
			readBOMMarker();
		} else {
			CharsetDecoder newdecoder = Charset.forName(cm.getName())
					.newDecoder();
			newdecoder.onMalformedInput(CodingErrorAction.REPLACE);

			fileReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(StringEscapeUtils
							.escapeJava(fileName)), newdecoder));
			// BOM marker will only appear on the very beginning
			readBOMMarker();
		}
		this.seperator = Pattern.quote(seperator);
	}

	public ReltioFlatFileReader(String fileName, String seperator,
			String decoder) throws IOException {

		CharsetDecoder newdecoder = Charset.forName(decoder).newDecoder();
		newdecoder.onMalformedInput(CodingErrorAction.REPLACE);
		fileReader = new BufferedReader(new InputStreamReader(
				new FileInputStream(StringEscapeUtils.escapeJava(fileName)),
				newdecoder));
		// BOM marker will only appear on the very beginning
		readBOMMarker();
		if (seperator != null) {
			this.seperator = Pattern.quote(seperator);
		} else {
			this.seperator = null;
		}
	}

	public ReltioFlatFileReader(String fileName) throws IOException {
		BufferedInputStream isr = new BufferedInputStream(new FileInputStream(
				StringEscapeUtils.escapeJava(fileName)));

		CharsetDetector charsetDetector = new CharsetDetector();
		charsetDetector.setText(isr);
		charsetDetector.enableInputFilter(true);
		CharsetMatch cm = charsetDetector.detect();
		System.out.println("Decorder of the File (CharSet) :: " + cm.getName());
		isr.close();

		if (Charset.availableCharsets().get(cm.getName()) == null
				|| cm.getName().equals("Shift_JIS")) {
			System.out
					.println("The "
							+ cm.getName()
							+ " charset not supported. So letting the reader to choose apporiate the Charset...");
			fileReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(
							StringEscapeUtils.escapeJava(fileName))));
			// BOM marker will only appear on the very beginning
			readBOMMarker();
		} else {
			CharsetDecoder newdecoder = Charset.forName(cm.getName())
					.newDecoder();
			newdecoder.onMalformedInput(CodingErrorAction.REPLACE);

			fileReader = new BufferedReader(
					new InputStreamReader(new FileInputStream(StringEscapeUtils
							.escapeJava(fileName)), newdecoder));
			// BOM marker will only appear on the very beginning
			readBOMMarker();
		}
		this.seperator = null;
	}

	@Override
	public String[] readLine() throws IOException {
		String line = fileReader.readLine();
		if (line != null) {
			if (seperator != null) {
				return line.split(seperator, -1);
			} else {
				String[] strArray = new String[1];
				strArray[0] = line;
				return strArray;
			}
		}

		return null;
	}

	@Override
	public void close() throws IOException {
		fileReader.close();

	}

	private void readBOMMarker() {
		try {
			fileReader.mark(4);
			if ('\ufeff' != fileReader.read())
				fileReader.reset(); // not the BOM marker
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
