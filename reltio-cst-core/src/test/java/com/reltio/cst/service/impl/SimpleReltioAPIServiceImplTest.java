package com.reltio.cst.service.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;
import com.reltio.cst.service.TokenGeneratorService;

@Ignore
public class SimpleReltioAPIServiceImplTest {

	private ReltioAPIService reltioAPIService;

	private static final String tenantURL = "https://" +TokenGeneratorServiceImplTest.testEnvironment+
			".reltio.com/reltio/api/"+TokenGeneratorServiceImplTest.testTenantId+
			"/entities/";

	@Before
	public void setUp() throws Exception {

		TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(
				TokenGeneratorServiceImplTest.username,
				TokenGeneratorServiceImplTest.password,
				TokenGeneratorServiceImplTest.validAuthUrl);
		reltioAPIService = new SimpleReltioAPIServiceImpl(tokenGeneratorService);
	}

	@Test
	public void testGetString_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService.get(tenantURL + "_total");
		System.out.println(response);
		Assert.assertNotNull(response);

	}

	@Test(expected = ReltioAPICallFailureException.class)
	public void testGetString_FailureInvalidURL()
			throws APICallFailureException, GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService.get(tenantURL + "_totalsss");
		System.out.println(response);
		Assert.assertNotNull(response);

	}

	@Test(expected = GenericException.class)
	public void testGetString_FailureInvalidHostname()
			throws APICallFailureException, GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService
				.get("https://devvvv.reltio.com/reltio/api/_totalsss");
		System.out.println(response);
		Assert.assertNotNull(response);

	}

	@Test
	public void testGetStringMapOfStringString_Sucesss()
			throws APICallFailureException, GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService.get(tenantURL + "_total", null);
		System.out.println(response);
		Assert.assertNotNull(response);
	}

	@Test
	public void testPostStringString_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService
				.post(tenantURL
						+ "_scan?filter=(equals(type,'configuration/entityTypes/Individual'))&select=uri&max=10",
						null);
		Assert.assertNotNull(response);
	}

	
	//@Test
	public void testDeleteStringString_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		String response = reltioAPIService
				.delete("https://"+TokenGeneratorServiceImplTest.testEnvironment+
								"-dataload.reltio.com/reltio/api/"+
								TokenGeneratorServiceImplTest.testTenantId+
								"/relations/"+TokenGeneratorServiceImplTest.notUsedCw1+
								"/crosswalks/"+TokenGeneratorServiceImplTest.notUsedCw2,
						null);
		System.out.println(response);
		Assert.assertNotNull(response);
	}
	

}
