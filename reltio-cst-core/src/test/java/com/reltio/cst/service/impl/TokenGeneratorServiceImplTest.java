package com.reltio.cst.service.impl;

import org.junit.Test;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.service.TokenGeneratorService;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

// Sample properties file for test (./test.properties)
// USERNAME = yourUsername
// PASSWORD = yourPassword
// VALID_AUTH_URL = https://auth.reltio.com/oauth/token

public class TokenGeneratorServiceImplTest {

	static {
		Properties props = new Properties();
		String propFileName = "test.properties";

		try {

			InputStream inputStream = new FileInputStream(propFileName);

			if (inputStream != null) {
				props.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
		} catch (FileNotFoundException e) {
			// File Not Found...
			System.out.println("File not found...");
		} catch (Exception e) {
			// Unknown Exception
			System.out.println("Unknown error...");
		}

		username = props.getProperty("USERNAME");
		password = props.getProperty("PASSWORD");
		validAuthUrl = props.getProperty("VALID_AUTH_URL");

		// Values below appear to not be used except for @ignore tests
		notUsedAuth1 = props.getProperty("NOT_USED_AUTH_1");
		notUsedCw1 = props.getProperty("NOT_USED_CW_1");
		notUsedCw2 = props.getProperty("NOT_USED_CW_2");
		testEnvironment = props.getProperty("TEST_ENVIRONMENT");
		testTenantId = props.getProperty("TEST_TENANT_ID");


	}

	public static final String username;
	public static final String password;
	public static final String validAuthUrl;

	public static final String notUsedAuth1;
	public static final String notUsedCw1;
	public static final String notUsedCw2;
	public static final String testEnvironment;
	public static final String testTenantId;

	@Test(expected = com.reltio.cst.exception.handler.GenericException.class)
	public void testTokenGneeratorInitiation_FailureWrongUrl()
			throws APICallFailureException, GenericException {
		new TokenGeneratorServiceImpl(username, password,
				"https://auth000.reltio.com/oauth/token");

	}

	@Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
	public void testTokenGneeratorInitiation_FailureWrongUsername()
			throws APICallFailureException, GenericException {
		new TokenGeneratorServiceImpl("dummy", password,
				validAuthUrl);

	}

	@Test
	public void testTokenGeneratorInitiation_success()
			throws APICallFailureException, GenericException {
		TokenGeneratorService tokenGeneratorService = new TokenGeneratorServiceImpl(
				username, password, validAuthUrl);

		String token = tokenGeneratorService.getToken();
		org.junit.Assert.assertNotNull(token);
		String newToken = tokenGeneratorService.getNewToken();
		org.junit.Assert.assertNotNull(newToken);
		Boolean isStart = tokenGeneratorService.startBackgroundTokenGenerator();
		org.junit.Assert.assertEquals(true, isStart);
		tokenGeneratorService.stopBackgroundTokenGenerator();

	}

}
