package com.reltio.cst.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.reltio.cst.exception.handler.APICallFailureException;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.RestAPIService;

@Ignore
public class SimpleRestAPIServiceImplTest {

	private RestAPIService restAPIService = new SimpleRestAPIServiceImpl();

	@Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
	public void testGet_APIcallFailure() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService
				.get("https://auth.reltio.com/oauth/token?username=test&password=test&grant_type=password",
						requestHeaders);

	}

	@Test(expected = com.reltio.cst.exception.handler.GenericException.class)
	public void testGet_InvalidURL() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService
				.get("https://auth222.reltio.com/oauth/token?username=test&password=test&grant_type=password",
						requestHeaders);

	}

	@Test(expected = com.reltio.cst.exception.handler.GenericException.class)
	public void testGet_NullURL() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService.get(null, requestHeaders);

	}

	@Test
	public void testGet_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		String response = restAPIService
				.get("https://auth.reltio.com/oauth/token?username="+TokenGeneratorServiceImplTest.username+
								"&password="+TokenGeneratorServiceImplTest.password+"&grant_type=password",
						requestHeaders);
		System.out.println(response);
		Assert.assertNotNull(response);
	}

	@Test(expected = com.reltio.cst.exception.handler.APICallFailureException.class)
	public void testPost_APIcallFailure() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService
				.post("https://auth.reltio.com/oauth/token?username=test&password=test&grant_type=password",
						requestHeaders, null);

	}

	@Test(expected = com.reltio.cst.exception.handler.GenericException.class)
	public void testPost_InvalidURL() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService
				.post("https://auth222.reltio.com/oauth/token?username=test&password=test&grant_type=password",
						requestHeaders, null);

	}

	@Test(expected = com.reltio.cst.exception.handler.GenericException.class)
	public void testPost_NullURL() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		restAPIService.post(null, requestHeaders, null);

	}

	@Test
	public void testPost_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		String response = restAPIService
				.post(TokenGeneratorServiceImplTest.notUsedAuth1,
						requestHeaders, null);
		System.out.println(response);
		Assert.assertNotNull(response);
	}
	
	@Test
	public void testDelete_Success() throws APICallFailureException,
			GenericException, ReltioAPICallFailureException {
		Map<String, String> requestHeaders = new HashMap<>();
		requestHeaders.put("Authorization", "Basic cmVsdGlvX3VpOm1ha2l0YQ==");

		String response = restAPIService
				.delete(TokenGeneratorServiceImplTest.notUsedAuth1,
						requestHeaders, null);
		System.out.println(response);
		Assert.assertNotNull(response);
	}

}
