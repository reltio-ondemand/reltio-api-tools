package com.reltio.file;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimpleFileWriterTest {

	public static void main(String[] args) throws IOException {

		//Step 1: Create Reltio File writer Reference
		ReltioFileWriter reltioFileWriter;
		
		//Step 2: Create a object based on the File Type.
		//if CSV then use below option
			//Step 2.1: Only with File Name using ReltioCSVFileWriter
			reltioFileWriter = new ReltioCSVFileWriter("<FileName>");
			
			//With File Name & Encoding technique like UTF-8 or any other
			reltioFileWriter = new ReltioCSVFileWriter("<FileName>", "UTF-8");
		
		//ELSE if FLAT FILE then use below option
			//Step 2.1: Only with File Name. Default Delimiter is pipe
			reltioFileWriter = new ReltioFlatFileWriter("<FileName>");
			
			//With File Name & Separator like pipe, Encoding
			reltioFileWriter = new ReltioFlatFileWriter("<FileName>","UTF-8");
			
			//With File Name, Separator & Encoding
			reltioFileWriter = new ReltioFlatFileWriter("<FileName>","UTF-8", "UTF-8");
	
	//Step 3: Write data to the File.
			//Step 3.1 Write line by line
			String[] line = null; //only one line
			reltioFileWriter.writeToFile(line);
			
			//Write Bulk of lines to the file
			List<String[]> lines = new ArrayList<>();
			reltioFileWriter.writeToFile(lines);
			
			//Write String data to the file (NOTE: only for Reltio Flat File writter)
			reltioFileWriter.writeToFile("<Flat File>");
			
			//Write Bulk of String line to the file (NOTE: only for Reltio Flat File writter)
			List<String> linesOfString = new ArrayList<>();
			linesOfString.add("<Flat File>");
			reltioFileWriter.writeBulkToFile(linesOfString);
			
	
	
	//Step 4: Close the stream for writting all the lines in the memory
	reltioFileWriter.close();
		
	}

}
