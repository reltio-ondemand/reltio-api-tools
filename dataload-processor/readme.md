```
#!txt

Copyright 2016 Reltio 100 Marine Parkway, Suite 275, Redwood Shores, CA USA 94065 (855) 360-DATA www.reltio.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
  
[TOC]




# Dataload Processor

Welcome to the Dataload Processor subproject!

## Purposes

The Reltio Dataload Processor is used to bulk-load data into the Reltio environment.

## Service architecture

The Reltio Dataload Processor is a java appliaction and operates over a collection of JSON formated data.

## Installation

You will install this project as a Maven project.  A recommended tool to handle this is the Intellij Java IDE.

Maven will automatically identify and attempt to locate dependencies through the .pom and .iml files.

Maven will not be able to locate the "reltio-cst-core" dependency.  For this you will need to create a separate maven project for the "reltio-cst-core" project.  You will need to install this new project into your Maven environment.

Once the "reltio-cst-core" project is installed to Maven, and Maven has located the dependencies, your environment is correctly set up.  You can not compile and package your program into a jar.

## Configuration

The following is an example configuration file for the Dataload Processor:

    RECORDS_PER_POST=5
    THREAD_COUNT=2
    JSON_FILE_PATH=C:\\My\\Project\\Space\\json.txt
    FAILED_RECORD_FILE_PATH=C:\\My\\Project\\Space\\failures.txt
    JSON_FILE_TYPE=PIPE_ARRAY
    DATALOAD_SERVER_HOST=mydataloadserver
    TENANT_ID=mytenantid
    DATALOAD_TYPE=Entities
    TYPE_OF_DATA=Contact
    USERNAME=myReltioUsername
    PASSWORD=myReltioPassword
    MAX_QUEUE_SIZE=300000
    AUTH_URL=authurl
    USER_COMMENTS=Made By Data Loader
    SKIP_PROCESS_TRACKER=FALSE
    EMAIL_IDS_TO_SEND_UPDATE=someone@test.com
    MAIL_SMTP_HOST = mysmtpserver
    FROM_EMAIL = "no-reply@test.com"
    FROM_EMAIL_NAME = "Test No-reply"
    DEFAULT_BCC_EMAIL = "someone@test.com"
    SMTP_USERNAME = mySmtpUsername
    SMTP_PASSWORD = mySmtpPassword

It is recommended to put the configuration file in the same directory as the jar will be executed from.

## Run the Program

To run the program you will open up your operating system's terminal.  You will navigate to the directory of the jar you created in the installation steps and enter the following:

    java -jar <jar name> <config name>

## Sample Data

You may use the following sample data to run tests of the Dataload Tool.

[Sample Data](https://bitbucket.org/reltio-ondemand/reltio-api-tools/src/master/dataload-processor/samples/sampleJson.txt?at=master&fileviewer=file-view-default)

# Additional information

For more information, please visit 

## [Reltio Help Desk](https://help.reltio.com/kb/865)





3. Running the Program

In order to run the program, you must have an up-to-date Java JRE.  You will navigate to the folder where you packaged your jar, and you will execute the following:

    java -jar (jar file name) (config file name)






