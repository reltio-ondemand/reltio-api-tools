/**
 * 
 */
package com.reltio.cst.dataload;

import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 *
 */
public final class DataloadConstants {


	public static final String PC_SOURCE_SYSTEM = "configuration/sources/Reltio";
	public static final Gson GSON = new Gson();
	public static final Integer MAX_FAILURE_COUNT = 1000000;
	public static final String FAILURE_LOG_KEY = "FailedToResentJson|";
	public static final Integer DEFAULT_ERROR_CODE = 000;
	public static final Integer INVALID_JSON_FILE = 001;
	public static final String INVALID_JSON_FILE_MESSAGE = "Invalid JSON in the input File";
	public static final Integer MAX_FAILURE_PER_ERROR_CODE = 10;
	public static final Integer MAX_QUEUE_SIZE = 500000;
	public static final String JSON_FILE_TYPE_PIPE = "PIPE_ARRAY";
	public static final String JSON_FILE_TYPE_ARRAY = "ARRAY";
	public static final String JSON_FILE_TYPE_OBJECT = "OBJECT";

	public static final Integer RECORDS_PER_POST = 30;
	public static final Integer THREAD_COUNT = 5;

	public static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
	public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
	public static final String MAIL_SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
	public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String TO_EMAIL = "toEmail";
	public static final String CC_EMAIL = "ccEmail";
	public static final String BCC_EMAIL = "bccEmail";
	public static final String SUBJECT_LINE = "subjectLine";
	public static final String MESSAGE_BODY = "messageBody";
	public static final String MIME_TYPE = "mimeType";
	public static final String TIME_TO_EXPIRE = "timeToExpire";


	public static final String[] FAILED_LOG_FILE_HEADER = { "Crosswalk Type",
			"Crosswalk Value", "Error Code", "Error Message",
			"Error Detailed Message" };

	// Start secure constants here
	// These are initialized at runtime from the properties file in cmd line args
	public static String MAIL_SMTP_HOST;
	public static String FROM_EMAIL;
	public static String FROM_EMAIL_NAME;
	public static String DEFAULT_BCC_EMAIL;
	public static String SMTP_USERNAME;
	public static String SMTP_PASSWORD;

}
